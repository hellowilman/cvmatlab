#include <stdlib.h>
#include <stdio.h>
#include "mex.h"
#include "cvmatlab.h"
#include <opencv/cv.h>
using namespace cv; 
using namespace std;
using namespace cvmatlab;

void printf(Mat x);
void printf64f(Mat x){
  for(int i=0; i<x.rows; i++){
      for(int j=0; j<x.cols;j++){
          printf("%f ", x.at<double>(i,j));
      }
      printf("\n");
    }
}
void printf32f(Mat x){
  for(int i=0; i<x.rows; i++){
      for(int j=0; j<x.cols;j++){
          printf("%f ", x.at<float>(i,j));
      }
      printf("\n");
    }
}
void printf8u(Mat x){
  for(int i=0; i<x.rows; i++){
      for(int j=0; j<x.cols;j++){
          printf("%d ", x.at<unsigned char>(i,j));
      }
      printf("\n");
    }
}
void printf8s(Mat x){
  for(int i=0; i<x.rows; i++){
      for(int j=0; j<x.cols;j++){
          printf("%d ", x.at<char>(i,j));
      }
      printf("\n");
    }
}
void printf(Mat x){
   if(x.type() == 0){
     printf8u(x);   
   }
   if(x.type() == 1){
     printf8s(x);   
   }
   if(x.type() == 6){
     printf64f(x);   
   }
   if(x.type() == 5){
     printf32f(x);   
   }
}
void mexFunction(int nlhs, mxArray* plhs[], int nrhs, const mxArray* prhs[]){
   if(nrhs>0){
     Mat outMat = cvmatlab::mxArray2Mat2D(prhs[0]);   
     printf(outMat);
     printf(":> step step[0]-%d step[1]-%d step1()-%d\n", outMat.step[0], outMat.step[1],outMat.step1());
     printf(":> step size: (%d %d) \n", outMat.rows, outMat.cols);
     printf(":> outMat type is %d\n", outMat.type());
     if(nlhs > 0)
     {
         printf("convert the nlhs (%d)\n", nlhs);
         mxArray *pOut = cvmatlab::Mat2D2mxArray(outMat);
         if(pOut){
             plhs[0] = pOut;
         }else{
             plhs[0] = mxCreateNumericMatrix(0,0,mxSINGLE_CLASS,mxREAL);
         }         
     }
   }
   printf("hello \n");   
}


