#include <opencv/cv.h>
#include "mex.h"
namespace cvmatlab{
    // convert 2D mat to mxArray 
    mxArray* Mat2D2mxArray(cv::Mat mat); 
    // convert mxArray to 2D mat 
    cv::Mat mxArray2Mat2D(const mxArray* data);
	
}