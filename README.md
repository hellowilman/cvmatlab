This project is going to provide a lite APIs for JS user to call opencv and matlab APIs from JS environment
# CVMatlab
This is a very very simple APIs to convert between mxArray and OpenCV Mat. 

# how to use it? 
Please refer to make.m and test.cpp. 
test.cpp gives a simple example to convert mxArray to Mat and Mat to mxArray
make.m gives how to build the program using CVMatlab 

# license
under LGPL 
