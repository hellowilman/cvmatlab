#include "cvmatlab.h"

#include <stdlib.h>
#include <stdio.h>

using namespace cv; 
using namespace std;

cv::Mat cvmatlab::mxArray2Mat2D(const mxArray* data){
    
    //; cvmatlab::mxArray2Mat2D(const mxArray* data,cv::Mat& outMat)
    int d = mxGetNumberOfDimensions(data);  
    // only support a 2D matrix 
    if(d !=2 ){
        return Mat();
    }
    int M = mxGetM(data);  // rows 
    int N = mxGetN(data);  // cols 
    
    int C = mxGetClassID(data); // the input ID 
    printf("class id is %d\n", mxGetClassID(data));
    Mat out = Mat();
    
    if(C == 6){
        Mat tmp(N,M,CV_64F,mxGetData(data)); out = tmp;
    }
    if(C == 7){
        Mat tmp(N,M,CV_32F,mxGetData(data)); out = tmp;
    }
    if( C == 8 || C == 3){
        Mat tmp(N,M,CV_8S,mxGetData(data)); out = tmp;
    }
    if( C == 9){
        Mat tmp(N,M,CV_8U,mxGetData(data)); out = tmp;
    }
    if( C == 9){
        Mat tmp(N,M,CV_8U,mxGetData(data)); out = tmp;
    }
    if( C == 10 ){
        Mat tmp(N,M,CV_16S,mxGetData(data)); out = tmp;    
    }
    if( C == 11 ){
        Mat tmp(N,M,CV_16U,mxGetData(data)); out = tmp;    
    }
    if( C == 12 ){
        Mat tmp(N,M,CV_32S,mxGetData(data)); out = tmp;    
    }
    if( C == 13 ){
        printf("Do not support unsigned 32 bit integer!\n");  
        out = Mat(); 
    }
    if(out.empty()){
        printf("return an empty matrix\n");
        return Mat();
    }else{
	    out = out.clone(); 
        out = out.t();
        return out; 
    }
       
};


mxArray* cvmatlab::Mat2D2mxArray(cv::Mat mat){
    if(mat.empty()){
        return  mxCreateNumericMatrix(0,0,mxDOUBLE_CLASS,mxREAL);
    }
    
    int M  = mat.rows;
    int N  = mat.cols;
    mxClassID type = mxSINGLE_CLASS;
/* opencv mat type 
#define CV_8U   0
#define CV_8S   1
#define CV_16U  2
#define CV_16S  3
#define CV_32S  4
#define CV_32F  5
#define CV_64F  6
#define CV_USRTYPE1 7 
*/
    mxArray* pData;
    mxClassID MAT_2_MX[] =  { 
        mxUINT8_CLASS , mxINT8_CLASS, mxUINT16_CLASS, mxINT16_CLASS, mxINT32_CLASS, 
        mxSINGLE_CLASS, mxDOUBLE_CLASS
    };
   pData = mxCreateNumericMatrix(M,N,MAT_2_MX[mat.type()],mxREAL);
   
   if(pData){
      mat = mat.clone(); 
      mat = mat.t();
      
      if(mat.step[0]/mat.step[1] == mat.cols){
         memcpy( mxGetData(pData), mat.data, M * N * (mat.step[1]) );    
      }else{
         printf("not support yet!\n");
         return  mxCreateNumericMatrix(0,0,mxDOUBLE_CLASS,mxREAL);
      }
   }else{
      printf("Cannot create numeric matrix\n");
      return  mxCreateNumericMatrix(0,0,mxDOUBLE_CLASS,mxREAL);   
   }
   return pData; 
}
